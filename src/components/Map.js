'use client'
import { useRef } from "react";
import { MapContainer, Marker, Popup, TileLayer } from "react-leaflet";

export default function Map() {
    const position = [51.505, -0.09];
    const mapRef = useRef();



    const displayPolyline = () => {
        var latlngs = [
            [45.51, -122.68],
            [37.77, -122.43],
            [34.04, -118.2]
        ];
        var polyline = L.polyline(latlngs, { color: 'red' }).addTo(mapRef.current);
        mapRef.current.fitBounds(polyline.getBounds());
    }
    const handleClick = (e) => {
        displayPolyline();
    }

    const handleChange = (e) => {
        displayPolyline();
    }

    return (
        <>
            <input onChange={handleChange} />
            <button onClick={handleClick}>ADD POLY</button>
            <div style={{ background: 'red' }}>
                <MapContainer
                    ref={mapRef}
                    center={position} zoom={13} scrollWheelZoom={false} style={{ height: '100vh', width: '100vw' }}>
                    <TileLayer
                        attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
                        url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                    />
                    <Marker position={position}>
                        <Popup>
                            A pretty CSS3 popup. <br /> Easily customizable.
                        </Popup>
                    </Marker>
                </MapContainer>
            </div>
        </>
    );
}
