'use client'
import { useRef } from "react";
import { MapContainer, Marker, Popup, TileLayer } from "react-leaflet";
import dynamic from 'next/dynamic'; // Composant LeafletMap chargé dynamiquement 

const Map = dynamic(() => import('@/components/Map'), { ssr: false }); 

export default function Page() {
  
    return (
        <>
         <Map />
        </>
    );
}
